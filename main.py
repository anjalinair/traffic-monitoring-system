#python main.py --input 2.mp4 --output output.mp4 --yolo yolo-coco
import numpy as np
import argparse
import imutils
import time
import cv2
import os
import glob
import math

files = glob.glob('output/*.png')
for f in files:
    os.remove(f)

from sort import *
tracker = Sort()
memory = {}
line1 = [(400,638), (1250, 788)]
line2 = [(120,838), (1120, 1080)]
counter1 = 0
counter2 = 0

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
    help="path to input video")
ap.add_argument("-o", "--output", required=True,
    help="path to output video")
ap.add_argument("-y", "--yolo", required=True,
    help="base path to YOLO directory")
ap.add_argument("-c", "--confidence", type=float, default=0.35,
    help="minimum probability to filter weak detections")
ap.add_argument("-t", "--threshold", type=float, default=0.25,
    help="threshold when applyong non-maxima suppression")
args = vars(ap.parse_args())


def adjust_gamma(image, gamma=1.0):
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
        for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image, table)

def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

def ccw(A,B,C):
    return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])

labelsPath = os.path.sep.join([args["yolo"] , "coco.names"])
LABELS = open(labelsPath).read().strip().split("\n")


np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(200, 3),
    dtype="uint8")


weightsPath = os.path.sep.join([args["yolo"], "yolov3.weights"])
configPath = os.path.sep.join([args["yolo"], "yolov3.cfg"])

print("[INFO] loading YOLO from disk...")
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)
ln = net.getLayerNames()
ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]


vs = cv2.VideoCapture(args["input"])
writer = None
(W, H) = (None, None)

frameIndex = 0
try:
    prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() \
        else cv2.CAP_PROP_FRAME_COUNT
    total = int(vs.get(prop))
    print("[INFO] {} total frames in video".format(total))

except:
    print("[INFO] could not determine # of frames in video")
    print("[INFO] no approx. completion time can be provided")
    total = -1

while True:
    (grabbed, frame) = vs.read()

    
    if not grabbed:
        break

    if W is None or H is None:
        (H, W) = frame.shape[:2]

    frame = adjust_gamma(frame, gamma=1.5)
    # YOLO LOADING

    blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (256, 256),
        swapRB=True, crop=False)
    net.setInput(blob)
    start = time.time()
    layerOutputs = net.forward(ln)
    end = time.time()


    boxes = []
    center = []
    confidences = []
    classIDs = []

    for output in layerOutputs:
 
        for detection in output:
            

            scores = detection[5:]
            classID = np.argmax(scores)
            confidence = scores[classID]

        
            if confidence > args["confidence"]:
          
                box = detection[0:4] * np.array([W, H, W, H])
                (centerX, centerY, width, height) = box.astype("int")
                
              
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                center.append(int(centerY))
                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence))
                classIDs.append(classID)
                
    idxs = cv2.dnn.NMSBoxes(boxes, confidences, args["confidence"], args["threshold"])
    #print("idxs", idxs)
    #print("boxes", boxes[i][0])
    #print("boxes", boxes[i][1])
    
    dets = []
    if len(idxs) > 0:
        for i in idxs.flatten():
            (x, y) = (boxes[i][0], boxes[i][1])
            (w, h) = (boxes[i][2], boxes[i][3])
            dets.append([x, y, x+w, y+h, confidences[i]])
            #print(confidences[i])
            #print(center[i])
    np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
    dets = np.asarray(dets)
    tracks = tracker.update(dets)
    
    boxes = []
    indexIDs = []
    c = []
    
    previous = memory.copy()
    #print("centerx",centerX)
    #  print("centery",centerY)
    memory = {}

    for track in tracks:
        boxes.append([track[0], track[1], track[2], track[3]])
        indexIDs.append(int(track[4]))
        memory[indexIDs[-1]] = boxes[-1]

    if len(boxes) > 0:
        i = int(0)
        for box in boxes:
            (x, y) = (int(box[0]), int(box[1]))
            (w, h) = (int(box[2]), int(box[3]))

            # draw a bounding box rectangle and label on the image
            # color = [int(c) for c in COLORS[classIDs[i]]]
            # cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)

            color = [int(c) for c in COLORS[indexIDs[i] % len(COLORS)]]
            cv2.rectangle(frame, (x, y), (w, h), color, 2)

            if indexIDs[i] in previous:
                previous_box = previous[indexIDs[i]]
                (x2, y2) = (int(previous_box[0]), int(previous_box[1]))
                (w2, h2) = (int(previous_box[2]), int(previous_box[3]))
                p0 = (int(x + (w-x)/2), int(y + (h-y)/2))
                p1 = (int(x2 + (w2-x2)/2), int(y2 + (h2-y2)/2))
                cv2.line(frame, p0, p1, color, 3)
                
                y_pix_dist = int(y + (h-y)/2) - int(y2 + (h2-y2)/2)
                text_y = "{} y".format(y_pix_dist)
                x_pix_dist = int(x + (w-x)/2) - int(x2 + (w2-x2)/2)
                text_x = "{} x".format(x_pix_dist)
                #cv2.putText(frame, text_y, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 4)
                #cv2.putText(frame, text_x, (x, y + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 4)
                final_pix_dist = math.sqrt((y_pix_dist*y_pix_dist)+(x_pix_dist*x_pix_dist))
                speed = np.round(1.5 * y_pix_dist,2)
                text_speed = "{} km/h".format(speed)
                cv2.putText(frame, text_speed, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
                

                if intersect(p0, p1, line1[0], line1[1]):
                    counter1 += 1
#                if intersect(p0, p1, line2[0], line2[1]):
#                    counter2 += 1

            # text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
            #text = "{}".format(indexIDs[i])
            #cv2.putText(frame, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
            i += 1

 
    cv2.line(frame, line1[0], line1[1], (0, 255, 255), 4)
#    cv2.line(frame, line2[0], line2[1], (255, 0, 255), 2)
    data=len(confidences)
    note_text =data
   

    print(data)
    waitt=(data/4)+9
    counter_text = "counter:{}".format(data)
    wait_text = "In Seconds:{}".format(waitt)
    
    cv2.putText(frame, counter_text, (100,250), cv2.FONT_HERSHEY_DUPLEX, 4.0, (0, 0, 255), 4)
    cv2.putText(frame, wait_text, (200,450), cv2.FONT_HERSHEY_DUPLEX, 4.0, (0, 0, 255), 4)
#    cv2.putText(frame, "ctr2",str(counter2), (100,400), cv2.FONT_HERSHEY_DUPLEX, 5.0, (255, 0, 255), 10)
    # counter += 1

    # saves image file
    #+cv2.imwrite("output/frame-{}.png".format(frameIndex), frame)

    # check if the video writer is None
    if writer is None:

        fourcc = cv2.VideoWriter_fourcc(*"MJPG")
        writer = cv2.VideoWriter(args["output"], fourcc, 15,
            (frame.shape[1], frame.shape[0]), True)

        if total > 0:
            elap = (end - start)
            print("[INFO] single frame took {:.4f} seconds".format(elap))
            print("[INFO] estimated total time to finish: {:.4f}".format(
                elap * total))

    writer.write(frame)

    frameIndex += 1

    #if frameIndex >= 4000: # limits the execution to the first 4000 frames
    #    print("[INFO] cleaning up...")
    #    writer.release()
    #    vs.release()
    #    exit()

cv2.putText(frame, wait_text, (100,250), cv2.FONT_HERSHEY_DUPLEX, 4.0, (0, 0, 255), 4)
print("[INFO] cleaning up...")
writer.release()
vs.release()
print(len(confidences))
os.system('python simulation.py')
